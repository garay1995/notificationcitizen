import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { notificationRequest } from '../models/notification/notification';
import { notificationsRequest } from '../models/notifications/notifications-request';


const API_URL = environment.URL_SERVICES;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class NotificationlistServicesService {

  constructor(private http: HttpClient) { }



  getNotificationList<T>(notificationRequest: notificationsRequest): Observable<T> {
    return this.http.post<any>(API_URL + "/notifications", notificationRequest, httpOptions).pipe(map(res => res));
  }

  getNotificationDetail<T>(notirequest: notificationRequest): Observable<T> {
    return this.http.post<any>(API_URL + "/notification", notirequest, httpOptions).pipe(map(res => res));
  }


}
