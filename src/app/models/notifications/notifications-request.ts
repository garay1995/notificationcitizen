export class notificationsRequest{
    search : string;
    filter : string;
    page   : number;
    count  : number;
}