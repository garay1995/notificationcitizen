export class notifications{
    id              : string;
    expedient       : string;
    notifier_area   : string;
    received_at     : string;
    read_at         : string;
}

export class notificationsRespone{
    success     : boolean;
    page        : number;
    count       : number;
    recordsTotal: number;
    Items        : notifications[];
}