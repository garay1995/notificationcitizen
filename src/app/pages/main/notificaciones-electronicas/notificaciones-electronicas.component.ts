import { Component, OnInit } from '@angular/core';
import { notificationsRespone } from 'src/app/models/notifications/notifications';
import { notificationsRequest } from 'src/app/models/notifications/notifications-request';
import { NotificationlistServicesService } from 'src/app/services/notificationlist-services.service';
import { Router } from '@angular/router';
import { FuncionesService } from 'src/app/services/funciones.service';

interface Filtro {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-notificaciones-electronicas',
  templateUrl: './notificaciones-electronicas.component.html',
  styleUrls: ['./notificaciones-electronicas.component.scss']
})
export class NotificacionesElectronicasComponent implements OnInit {

  notificationRespone : notificationsRespone = new notificationsRespone();
  notificationRequest : notificationsRequest = new notificationsRequest();
  listReadyCheck: boolean;
  mensaje  : string;
  textSearch :string ='';

  pageIndex :number ;
  pageSize :number;

  filterSelected: string = '0';

  constructor(private funcionesService:FuncionesService,  private notificationsservices : NotificationlistServicesService, private router: Router) {

    this.pageIndex =1;
    this.pageSize = 5;
   }

  ngOnInit(): void {
    this.GetNotificationList(this.textSearch,"",this.pageIndex,this.pageSize);
  }

  filtros: Filtro[] = [
    {value: '0', viewValue: 'Todos'},
    {value: '1', viewValue: 'Leído'},
    {value: '2', viewValue: 'No Leído'}
  ];


  GetNotificationList(uerySearch: string,filter : string, page: number , pagesize :number){
    this.listReadyCheck = false;
    this.notificationRequest.search = uerySearch;
    this.notificationRequest.filter =filter;
    this.notificationRequest.page =page;
    this.notificationRequest.count =pagesize;

    this.notificationsservices.getNotificationList<any>(this.notificationRequest)
    .subscribe(
      data => {
        if (data.success){
          this.listReadyCheck = true;
          this.notificationRespone = data;
        }else{
         this.mensaje=data.error.message;

         this.funcionesService.mensajeError(this.mensaje);

        }
      },
      error => {
        this.funcionesService.mensajeError('Problemas en el servicio.');
      }
    );
  }


  goNotificationDetail(item: any) {
		this.router.routeReuseStrategy.shouldReuseRoute = () => false;
		this.router.navigate(['/main/notificaciones-electronicas-detalle/' + item]);
	}


  onOptionsSelected() {    
    this.GetNotificationList(this.textSearch, this.filterSelected, this.pageIndex,this.pageSize);
  }

getColor(nombre :string){
  var letra = nombre.substring(-1,1);
  if(letra === 'A' ||letra === 'B' || letra === 'C'){
    return 'listCircle rdmColor_1';
  }else if(letra === 'D' ||letra === 'E' || letra === 'F'){
    return 'listCircle rdmColor_2';
  }else if(letra === 'G' ||letra === 'H' || letra === 'I'){
    return 'listCircle rdmColor_3';
  }else if(letra === 'J' ||letra === 'K' || letra === 'L'){
    return 'listCircle rdmColor_4';
  }else if(letra === 'M' ||letra === 'N' || letra === 'Ñ'){
    return 'listCircle rdmColor_5';
  }else if(letra === 'O' ||letra === 'P' || letra === 'Q'){
    return 'listCircle rdmColor_6';
  }else if(letra === 'R' ||letra === 'S' || letra === 'T'){
    return 'listCircle rdmColor_7';
  }else if(letra === 'U' ||letra === 'V' || letra === 'W'){
    return 'listCircle rdmColor_8';
  }else if(letra === 'X' ||letra === 'Y' || letra === 'Z'){
    return 'listCircle rdmColor_9';
  }
}

pageChangeEvent(event) {
  this.pageIndex = event.pageIndex + 1;
  this.pageSize = event.pageSize;

  this.GetNotificationList(this.textSearch,this.filterSelected, this.pageIndex, this.pageSize);
}

searchByQuery() {
  this.GetNotificationList(this.textSearch,this.filterSelected, this.pageIndex, this.pageSize);
}

}



