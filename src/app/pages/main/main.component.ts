import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { notificationsRequest } from 'src/app/models/notifications/notifications-request';
import { NotificationlistServicesService } from 'src/app/services/notificationlist-services.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  notificationRequest: notificationsRequest = new notificationsRequest();
  contador: string ="0";
  constructor(private router: Router,private notificationsservices : NotificationlistServicesService) {
   }

  ngOnInit(): void {
    this.countnotview();
  }

  linkRedirect(section: any) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.navigate(['/main']);
  }


  countnotview(){
    this.notificationRequest.search = "";
    this.notificationRequest.filter ="2";
    this.notificationRequest.page =1;
    this.notificationRequest.count =5;
    this.notificationsservices.getNotificationList<any>(this.notificationRequest)
    .subscribe(
      data => {
        if (data.success){
          this.contador = data.recordsTotal;
        }
      },
      error => {
       console.log('Problemas en el servicio.');
      }
    );
  }
}
