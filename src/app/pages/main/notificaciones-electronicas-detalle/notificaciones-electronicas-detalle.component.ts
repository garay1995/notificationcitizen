import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { attachment, notification, notificationRequest } from 'src/app/models/notification/notification';
import { notificationsRequest } from 'src/app/models/notifications/notifications-request';
import { NotificationlistServicesService } from 'src/app/services/notificationlist-services.service';
@Component({
  selector: 'app-notificaciones-electronicas-detalle',
  templateUrl: './notificaciones-electronicas-detalle.component.html',
  styleUrls: ['./notificaciones-electronicas-detalle.component.scss']
})
export class NotificacionesElectronicasDetalleComponent implements OnInit {
  notiRequest: notificationRequest = new notificationRequest();
  notificationRequest: notificationsRequest = new notificationsRequest();
  id: string;
  public formulario: FormGroup;
  adjuntos: attachment[];
  view1: string = 'color_1 posUno';
 
  view2: string = 'color_2 posDos';
  icon2: string = 'stop_circle';

  urlAcuse: string;

  @Input() numNotview: number;

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationsservices : NotificationlistServicesService,
    private router: Router) {
      this.id = this.route.snapshot.paramMap.get('id');
   }

  ngOnInit(): void {
    this.createForm();
    this.GetNotificationDetail();
  }

  createForm(): void{
    this.formulario = this.fb.group({
      n_expediente :[''],
      n_notifier_Area :[''],
      n_received_at :[''],
      n_read_at  :[''],
      n_message :['']
    });
  }


  GetNotificationDetail(){
    this.notiRequest.id = this.id;

    this.notificationsservices.getNotificationDetail<any>(this.notiRequest)
    .subscribe(
      data => {
        if (data.success){
          console.log("DATA", data)
          this.loadNotificationData(data.notification);
          this.view2  = 'color_1 posDos';
          this.icon2  = 'check_circle';
        }
      });
  }

  loadNotificationData(noti :notification){
    this.formulario.get("n_expediente").setValue(noti.expedient);
    this.formulario.get("n_notifier_Area").setValue(noti.notifier_area);
    this.formulario.get("n_received_at").setValue(noti.received_at);
    this.formulario.get("n_read_at").setValue(noti.read_at);
    this.formulario.get("n_message").setValue(noti.message);
    this.urlAcuse = noti.acuse.url;
    this.adjuntos = noti.attachments;
  }

  viewDocument(item: any){
    window.open(item.url, '_blank');
  }

  downloadAcuse(){
     window.open(this.urlAcuse, '_blank');
  }

  print() {
    let printContents = document.getElementById('imp1').innerHTML;
    let contenido= document.getElementById('imp1').innerHTML;
    let contenidoOriginal= document.body.innerHTML;
    document.body.innerHTML = contenido;
    window.print();
    document.body.innerHTML = contenidoOriginal;
    location.reload();
  }


}
