import {Component, OnInit} from '@angular/core';
import {SeguridadService} from '../../services/seguridad.service';
import {Usuario} from '../../models/user/Usuario';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  existeSession: boolean;
  usuario: Usuario = new Usuario();

  constructor(
    private seguridadService: SeguridadService
  ) {

  }

  async ngOnInit() {
    this.existeSession = await this.seguridadService.verificarSesion();
    this.usuario.nombres ="prueba";
    this.usuario.apellidos ="apeprueba"
    this.usuario.rol="pruebarol"
  }

  cerrarSession() {
    this.seguridadService.cerrarSesion();
  }
}
